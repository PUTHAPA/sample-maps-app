package com.poi.samplemapsapp.action;

import java.util.Base64;
import java.util.Map;

import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.SessionAware;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.ActionSupport;
import com.poi.samplemapsapp.entity.User;
import com.poi.samplemapsapp.service.LoginService;

@Component
public class LoginAction extends ActionSupport implements SessionAware {

	private static final long serialVersionUID = -172432987526619592L;
	
	private User user;
	private String name, password, message;
	private SessionMap<String, Object> sessionMap;
	
	@Autowired
	private LoginService loginService;
    
    public String execute() {
    	user = new User();
    	user.setName(name);
    	user.setPassword(password);
    	getUser().setPassword(Base64.getEncoder().encodeToString(getUser().getPassword().getBytes()));
    	User loggedUser = loginService.login(getUser());
    	if (loggedUser != null) {
    		loggedUser.setPassword(null);
    		user.setPassword(null);
    		sessionMap.put("loggedUser", loggedUser);
    		setMessage("SUCCESS");
    		return "HOME";
    	} else {
    		user = new User();
    		setMessage("ERROR");
    		return "LOGIN";
    	}
    }

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public void setSession(Map<String, Object> map) {
		sessionMap = (SessionMap<String, Object>) map;
	}
	

	public String doLogout() {
		sessionMap.remove("loggedUser");
		sessionMap.invalidate();
		return "LOGIN";
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
}
