<%@ page language="java" contentType="text/html; charset=US-ASCII"
	pageEncoding="US-ASCII"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/struts-tags" prefix="s"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=US-ASCII">
<title>Login Page</title>
<link rel="stylesheet" href="css/bootstrap.min.css">
<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
<script type="text/javascript">
	function login() {
		var name = $('#name').val();
		var password = $('#password').val();
		$.ajax({
			type : "POST",
			url : "login.action",
			data : "name=" + name + "&password=" + password,
			success : function(result) {
				if (result.message === "SUCCESS") {
					window.location = "dashboard.jsp";
				} else {
					$("#login-alert").removeAttr("style").append(
							"Invalid username or password");
					$('#name').val('');
					$('#password').val('');
				}
			},
			error : function(result) {
				alert("Some error occured.");
			}
		});
	}
</script>
</head>
<body>
	<div id="loginbox" style="margin-top: 100px; margin-left: 400px;"
		class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
		<div class="panel panel-info">
			<div class="panel-heading">
				<div class="panel-title">Sign In</div>
			</div>

			<div style="padding-top: 30px" class="panel-body">

				<div style="display: none;" id="login-alert"
					class="alert alert-danger col-sm-12"></div>

				<form id="loginform" class="form-horizontal" role="form">

					<div style="margin-bottom: 25px" class="input-group">
						<span class="input-group-addon"><i
							class="glyphicon glyphicon-user"></i></span> <input id="name"
							class="form-control" name="name" value="" placeholder="name"
							type="text">
					</div>

					<div style="margin-bottom: 25px" class="input-group">
						<span class="input-group-addon"><i
							class="glyphicon glyphicon-lock"></i></span> <input id="password"
							class="form-control" name="password" placeholder="password"
							type="password">
					</div>


					<div style="margin-top: 10px" class="form-group">
						<!-- Button -->

						<div class="col-sm-12 controls">
							<a id="btn-login" href="#" class="btn btn-success"
								onclick="login();">Login </a>

						</div>
					</div>
				</form>

			</div>
		</div>
	</div>

</body>
</html>