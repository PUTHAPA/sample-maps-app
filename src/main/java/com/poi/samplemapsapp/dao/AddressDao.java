package com.poi.samplemapsapp.dao;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.poi.samplemapsapp.dao.generic.GenericDaoHibernateImpl;
import com.poi.samplemapsapp.entity.Address;

@Repository
public class AddressDao extends GenericDaoHibernateImpl<Address, Serializable> {

	@SuppressWarnings("unchecked")
	public List<Address> getAddressById(final Long id) {
		final String hql = "from Address entity where entity.id=:id";
		Query query = currentSession().createQuery(hql);
		query.setParameter("id", id);
		return query.getResultList();
	}

}
