package com.poi.samplemapsapp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.poi.samplemapsapp.dao.PointOfInterestDao;
import com.poi.samplemapsapp.entity.PointOfInterest;

@Service
@Transactional
public class PointOfInterestService {

	@Autowired
	private PointOfInterestDao pointOfInterestDao;

	public List<PointOfInterest> getAllPointsOfInterestList() {
		return pointOfInterestDao.getAllPointsOfInterestList();
	}
}
