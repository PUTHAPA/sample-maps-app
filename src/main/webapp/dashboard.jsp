<%@taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=US-ASCII">
<title>Home Page</title>
<link rel="stylesheet" href="css/bootstrap.min.css">
<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
<script type="text/javascript">
	var selectedPoiAddressIdList = [];
	var pointOfInterestList;
	var locationData;
	function loadData() {
		$.ajax({
			type : "GET",
			url : "home.action",
			success : function(result) {
				pointOfInterestList = result.pointOfInterestList;
				locationData = result.location;
				var tblData = "";
				$.each(result.pointOfInterestList, function() {
					tblData += "<tr><td>" + this.name + "</td>" + "<td>"
							+ this.address.fullAddress + "</td>"
							+ "<td><input type='checkbox' value='"
							+ this.selectPointsOfInterest
							+ "' onChange='checkPoi(" + this.address.id
							+ ")'/></td>" + "</tr>";
				});
				$("#tbody").html(tblData);
				var location = "<p><b>Location:</b> " + result.location.name
						+ "</p><p><b>Location Address:</b> "
						+ result.location.address.fullAddress + "</p>";
				$("#location").html(location);
			},
			error : function(result) {
				alert("Some error occured.");
			}
		});
	}

	function checkPoi(addressId) {
		var index = selectedPoiAddressIdList.indexOf(addressId);
		if (index === -1) {
			selectedPoiAddressIdList.push(addressId);
		} else {
			selectedPoiAddressIdList.splice(index, 1);
		}
	}
	
	var rad = function(x) {
		  return x * Math.PI / 180;
	}

	var getDistance = function(p1, p2) {
		  var R = 6378137; 
		  var dLat = rad((p2.lat - p1.lat));
		  var dLong = rad((p2.lng - p1.lng));
		  var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
		    Math.cos(rad(p1.lat)) * Math.cos(rad(p2.lat)) *
		    Math.sin(dLong / 2) * Math.sin(dLong / 2);
		  return ((R * 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a)))/1000).toFixed(2); // returns the distance in meter
	}

	function initMap() {

		if (locationData) {
			//Map options
			var options = {
				zoom : 12,
				center : {
					lat : locationData.address.latitude,
					lng : locationData.address.longitude
				}
			}

			//New Map
			var map = new google.maps.Map(document.getElementById('map'),
					options);
			var center = {
					lat : locationData.address.latitude,
					lng : locationData.address.longitude
				};
			var markers = [];
			markers.push({
				coords : center,
				content : '<h5>Location: ' + locationData.name
						+ ' <br> Address: ' + locationData.address.fullAddress
						+ '</h5>'
			});

			for (var i = 0; i < selectedPoiAddressIdList.length; i++) {
				for (var j = 0; j < pointOfInterestList.length; j++) {
					if (selectedPoiAddressIdList[i] === pointOfInterestList[j].address.id) {
						var poi = pointOfInterestList[j];
						var coords = {
								lat : poi.address.latitude,
								lng : poi.address.longitude
							};
						markers.push({
							coords : coords,
							content : '<h5>Points of Interest: ' + poi.name
									+ ' <br> Address: '
									+ poi.address.fullAddress 
									+ '<br> Distance: ' + getDistance(coords, center)
									+ ' Km</h5>'
						});
					}
				}
			}

			for (var i = 0; i < markers.length; i++) {
				addMarker(markers[i]);
			}

			function addMarker(props) {
				//Add Marker
				var marker = new google.maps.Marker({
					position : props.coords,
					map : map
				});

				//add popup
				if (props.content) {
					var infoWindow = new google.maps.InfoWindow({
						content : props.content
					});

					marker.addListener('click', function() {
						infoWindow.open(map, marker);
					});
				}
			}
		}
	}
	
	function logout() {
		$.ajax({
			type : "POST",
			url : "logout.action",
			success : function(result) {
				window.location = "login.jsp";
			},
			error : function(result) {
				alert("Some error occured.");
			}
		});
	}
</script>
</head>
<body onload="loadData();">

	<nav class="navbar navbar-expand-lg navbar-light bg-light">
		<a class="navbar-brand" href="#">Sample Maps App</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#navbarSupportedContent"
			aria-controls="navbarSupportedContent" aria-expanded="false"
			aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			Welcome User: <b><s:property value="#session.loggedUser.name" />
			</b> |
			<button onclick="logout();" class="btn btn-primary">Logout</button>
		</div>
	</nav>
	<div class="card">
		<div class="container">
			<span id="location"></span>
			<table class="table table-bordered">
				<thead>
					<tr class="bg-info">
						<th>Name</th>
						<th>Address</th>
						<th>Select</th>
					</tr>
				</thead>
				<tbody id="tbody">
				</tbody>
			</table>
			<button id="go" onclick="initMap()" class="btn btn-primary">Go</button>
			<br>
			<br>
			<div id="map" style="width: 1024px; height: 400px;"></div>
			<script async defer
				src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBYu9msCN5Iyj664LB7HzawmEFd7m0cduQ&callback=initMap&language=en&oe=utf-8">
		</script>
		</div>
	</div>
</body>
</html>