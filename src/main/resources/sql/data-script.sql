--
-- TOC entry 2182 (class 0 OID 16583)
-- Dependencies: 197
-- Data for Name: address; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.address (id, city, country, latitude, longitude, number, street, zipcode) VALUES (1, 'Berlin', 'Germany', 52.527203999999998, 13.423665, '90', 'Otto-Braun-Strasse', '10249');
INSERT INTO public.address (id, city, country, latitude, longitude, number, street, zipcode) VALUES (3, 'Berlin', 'Germany', 52.525120999999999, 13.369401999999999, '1', 'Europaplatz', '10557');
INSERT INTO public.address (id, city, country, latitude, longitude, number, street, zipcode) VALUES (4, 'Berlin', 'Germany', 52.518645999999997, 13.376187, '1', 'Platz der Republik', '11011');
INSERT INTO public.address (id, city, country, latitude, longitude, number, street, zipcode) VALUES (2, 'Berlin', 'Germany', 52.558861999999998, 13.288385999999999, 'Tegel', 'Zufahrt zum Flughafen', '13405');
INSERT INTO public.address (id, city, country, latitude, longitude, number, street, zipcode) VALUES (5, 'Berlin', 'Germany', 52.516280000000002, 13.377703, 'Platz', 'Pariser', '10117');
INSERT INTO public.address (id, city, country, latitude, longitude, number, street, zipcode) VALUES (6, 'Berlin', 'Germany', 52.507471000000002, 13.390385999999999, '43-45', 'Friedrichstrasse', '10117');


--
-- TOC entry 2184 (class 0 OID 16594)
-- Dependencies: 199
-- Data for Name: location; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.location (id, name, address_id) VALUES (1, 'Leonardo Royal Hotel Berlin Alexanderplatz', 1);


--
-- TOC entry 2186 (class 0 OID 16602)
-- Dependencies: 201
-- Data for Name: point_of_interest; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.point_of_interest (id, name, address_id) VALUES (1, 'Airport Berlin-Tegel', 2);
INSERT INTO public.point_of_interest (id, name, address_id) VALUES (2, 'Berlin Main Train Station', 3);
INSERT INTO public.point_of_interest (id, name, address_id) VALUES (3, 'German parliament building', 4);
INSERT INTO public.point_of_interest (id, name, address_id) VALUES (4, 'Brandenburg Gate', 5);
INSERT INTO public.point_of_interest (id, name, address_id) VALUES (5, 'Checkpoint Charlie', 6);


--
-- TOC entry 2188 (class 0 OID 16610)
-- Dependencies: 203
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.users (id, name, password) VALUES (1, 'pushpa', 'cHVzaHBh');


--
-- TOC entry 2194 (class 0 OID 0)
-- Dependencies: 196
-- Name: address_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.address_id_seq', 6, true);


--
-- TOC entry 2195 (class 0 OID 0)
-- Dependencies: 198
-- Name: location_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.location_id_seq', 4, true);


--
-- TOC entry 2196 (class 0 OID 0)
-- Dependencies: 200
-- Name: point_of_interest_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.point_of_interest_id_seq', 5, true);


--
-- TOC entry 2197 (class 0 OID 0)
-- Dependencies: 202
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.users_id_seq', 1, true);


-- Completed on 2018-06-30 18:32:41

--
-- PostgreSQL database dump complete
