package com.poi.samplemapsapp.dao;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.poi.samplemapsapp.dao.generic.GenericDaoHibernateImpl;
import com.poi.samplemapsapp.entity.Location;

@Repository
public class LocationDao extends GenericDaoHibernateImpl<Location, Serializable> {

	@SuppressWarnings("unchecked")
	public Location getLocation() {
		final String hql = "from Location entity";
		Query query = currentSession().createQuery(hql);
		List<Location> result = query.getResultList();
		if (result.isEmpty()) {
			return new Location();
		} else {
			Location location = result.get(0);
			return location;
		}
	}

}
