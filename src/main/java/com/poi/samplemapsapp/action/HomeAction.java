package com.poi.samplemapsapp.action;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.ActionSupport;
import com.poi.samplemapsapp.entity.Address;
import com.poi.samplemapsapp.entity.Location;
import com.poi.samplemapsapp.entity.PointOfInterest;
import com.poi.samplemapsapp.service.AddressService;
import com.poi.samplemapsapp.service.LocationService;
import com.poi.samplemapsapp.service.PointOfInterestService;

@Component
public class HomeAction extends ActionSupport {

	private static final long serialVersionUID = -144374616075462328L;

	@Autowired
	private LocationService locationService;
	@Autowired
	private PointOfInterestService pointOfInterestService;
	@Autowired
	private AddressService addressService;

	private Location location;
	private List<PointOfInterest> pointOfInterestList;

	public String execute() {
		location = locationService.getLocation();
		List<Address> addressList = addressService.getAddressById(location.getAddress().getId());
		location.setAddress(addressList.get(0));
		
		pointOfInterestList = pointOfInterestService.getAllPointsOfInterestList();
		return "HOME";
	}

	public Location getLocation() {
		if (location == null) {
			location = new Location();
		}
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public List<PointOfInterest> getPointOfInterestList() {
		return pointOfInterestList;
	}

	public void setPointOfInterestList(List<PointOfInterest> pointOfInterestList) {
		this.pointOfInterestList = pointOfInterestList;
	}
}
