package com.poi.samplemapsapp.service;

import org.springframework.transaction.annotation.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.poi.samplemapsapp.dao.LoginDao;
import com.poi.samplemapsapp.entity.User;

@Service
@Transactional
public class LoginService {

	@Autowired
	private LoginDao loginDao;

	public User login(final User user) {
		return loginDao.login(user);
	}
}
