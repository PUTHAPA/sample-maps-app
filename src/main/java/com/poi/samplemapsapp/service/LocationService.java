package com.poi.samplemapsapp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.poi.samplemapsapp.dao.LocationDao;
import com.poi.samplemapsapp.entity.Location;

@Service
@Transactional
public class LocationService {
	
	@Autowired
	private LocationDao locationDao;

	public Location getLocation() {
		return locationDao.getLocation();
	}
	
}
