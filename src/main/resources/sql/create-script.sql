-- Database: "sample-maps-app-db"

DROP DATABASE "sample-maps-app-db";

CREATE DATABASE "sample-maps-app-db"
  WITH OWNER = postgres
       ENCODING = 'UTF8'
       TABLESPACE = pg_default
       LC_COLLATE = 'English_United States.1252'
       LC_CTYPE = 'English_United States.1252'
       CONNECTION LIMIT = -1;

DROP SEQUENCE public.users_id_seq;

CREATE SEQUENCE public.users_id_seq
  INCREMENT 0
  MINVALUE 0
  MAXVALUE 0
  START 0
  CACHE 0;
ALTER TABLE public.users_id_seq
  OWNER TO postgres;

DROP TABLE public.users;

CREATE TABLE public.users
(
  id bigint NOT NULL DEFAULT nextval('users_id_seq'::regclass),
  name character varying(255),
  password character varying(255),
  CONSTRAINT users_pkey PRIMARY KEY (id),
  CONSTRAINT uk_3g1j96g94xpk3lpxl2qbl985x UNIQUE (name)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.users
  OWNER TO postgres;

DROP SEQUENCE public.address_id_seq;

CREATE SEQUENCE public.address_id_seq
  INCREMENT 0
  MINVALUE 0
  MAXVALUE 0
  START 0
  CACHE 0;
ALTER TABLE public.address_id_seq
  OWNER TO postgres;
  
DROP TABLE public.address;

CREATE TABLE public.address
(
  id bigint NOT NULL DEFAULT nextval('address_id_seq'::regclass),
  city character varying(255),
  country character varying(255),
  latitude double precision,
  longitude double precision,
  "number" character varying(255),
  street character varying(255),
  zipcode character varying(255),
  CONSTRAINT address_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.address
  OWNER TO postgres;
  
DROP SEQUENCE public.location_id_seq;

CREATE SEQUENCE public.location_id_seq
  INCREMENT 0
  MINVALUE 0
  MAXVALUE 0
  START 0
  CACHE 0;
ALTER TABLE public.location_id_seq
  OWNER TO postgres;
  
DROP TABLE public.location;

CREATE TABLE public.location
(
  id bigint NOT NULL DEFAULT nextval('location_id_seq'::regclass),
  name character varying(255),
  address_id bigint,
  CONSTRAINT location_pkey PRIMARY KEY (id),
  CONSTRAINT fkt8psi9b5mkkfc0r9fgptngwhg FOREIGN KEY (address_id)
      REFERENCES public.address (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.location
  OWNER TO postgres;
  
DROP SEQUENCE public.point_of_interest_id_seq;

CREATE SEQUENCE public.point_of_interest_id_seq
  INCREMENT 0
  MINVALUE 0
  MAXVALUE 0
  START 0
  CACHE 0;
ALTER TABLE public.point_of_interest_id_seq
  OWNER TO postgres;

DROP TABLE public.point_of_interest;

CREATE TABLE public.point_of_interest
(
  id bigint NOT NULL DEFAULT nextval('point_of_interest_id_seq'::regclass),
  name character varying(255),
  address_id bigint,
  CONSTRAINT point_of_interest_pkey PRIMARY KEY (id),
  CONSTRAINT fkmxmej97wkt78hei2167b684k7 FOREIGN KEY (address_id)
      REFERENCES public.address (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.point_of_interest
  OWNER TO postgres;