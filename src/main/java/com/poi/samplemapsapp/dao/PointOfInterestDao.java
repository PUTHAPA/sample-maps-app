package com.poi.samplemapsapp.dao;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.poi.samplemapsapp.dao.generic.GenericDaoHibernateImpl;
import com.poi.samplemapsapp.entity.Address;
import com.poi.samplemapsapp.entity.PointOfInterest;

@Repository
public class PointOfInterestDao extends GenericDaoHibernateImpl<PointOfInterest, Serializable> {

	public List<PointOfInterest> getAllPointsOfInterestList() {
		final String sql = "select p.id as pId, p.name, a.id as aId, a.street, a.number, a.zipcode, a.city, a.country, "
				+ "a.latitude, a.longitude from point_of_interest p inner join address a on p.address_id = a.id";
		Query query = currentSession().createNativeQuery(sql);
		List<PointOfInterest> pointOfInterestList = new ArrayList<>();
		for (Object o : query.getResultList()) {
			Object[] obj = (Object[]) o;
			Address address = new Address();
			address.setId(((BigInteger) obj[2]).longValue());
			address.setStreet((String) obj[3]);
			address.setNumber((String) obj[4]);
			address.setZipcode((String) obj[5]);
			address.setCity((String) obj[6]);
			address.setCountry((String) obj[7]);
			address.setLatitude((Double) obj[8]);
			address.setLongitude((Double) obj[9]);
			PointOfInterest pointOfInterest = new PointOfInterest();
			pointOfInterest.setId(((BigInteger) obj[0]).longValue());
			pointOfInterest.setName((String) obj[1]);
			pointOfInterest.setAddress(address);
			pointOfInterestList.add(pointOfInterest);
		}
	    return pointOfInterestList;
	}

}
