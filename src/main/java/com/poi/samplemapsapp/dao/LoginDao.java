package com.poi.samplemapsapp.dao;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.poi.samplemapsapp.dao.generic.GenericDaoHibernateImpl;
import com.poi.samplemapsapp.entity.User;

@Repository
public class LoginDao extends GenericDaoHibernateImpl<User, Serializable> {

	@SuppressWarnings("unchecked")
	public User login(final User user) {
		final String hql = "from User entity where name=:name and password=:password";
		Query query = currentSession().createQuery(hql);
		query.setParameter("name", user.getName());
		query.setParameter("password", user.getPassword());
		List<User> result = query.getResultList();
		if (result.isEmpty()) {
			return null;
		} else {
			User loggedUser = result.get(0);
			return loggedUser;
		}
	}
}
